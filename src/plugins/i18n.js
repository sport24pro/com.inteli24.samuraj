// import Vue from 'vue';
// import store from '../store';
import { createI18n } from 'vue-i18n'
//import Cookies from 'js-cookie'

//
// const locale_pl = import '../lang/pl.json';
// const locale_en = import '../lang/en.json';
// const locale_ukr = import '../lang/ukr.json';


//Vue.use(VueI18n);
const i18n = createI18n({
    mode: 'legacy',
    legacy: true,
    locale: 'pl',
    messages: {
        'pl': {

            network: {
              error_title: 'Coś poszło nie tak',
              error_content: 'Połączenie z siecią nie było możliwe. Proszę upewnij się, że Twoje WiFi lub transmisja dnych w telefonie jest włączona lub spróbuj ponownie.',
              retry: 'Ponów',
              ok: 'OK',
            },

            product: {
                price: [
                    '{0} zł',
                    'cena ustalana indywidaulanie',
                    'produkt bezpłatny'
                ]
            },



            actions: {
                cancel: 'Anuluj',
                ok: 'OK',
                choose: 'Wybierz',
                back: 'Powrót'
            },

            exams: {
                exams: 'Egzaminy',
                add: 'Zapisz na egzamin',
                from_level: 'Zdaje z pasa',
                to_level: 'Na pas',
                note: 'Uwagi',
            },


            contract: {
                'end_of_contract': 'Zakończenie umowy',
                'termination_notice_at': 'Złożenie wypowiedzenia',
            },


            'calendar': {
                'day_activity': 'Dni zajęć:',
                'activity_one': 'Zajęcia w dniu',
                'activity_all': 'Tylko wybrne dni',
                week: ['niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota'],
                weeks: ['niedziele', 'poniedziałki', 'wtorki', 'środy', 'czwartki', 'piątki', 'soboty'],

                weeks_d: ['niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota'],
                months: ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"],
            },


            'form': {
                error: 'Nie zapisano formularza. Proszę poprawić pola zaznaczone na czerwono.',
                save: 'Zapisz',
                edit: 'Zmień dane',
                success: 'Dane zostały zapisane'
            },

            attributes: {
                name: 'Imię',
                surname: 'Nazwisko',
                phone: 'Numer telefonu',
                email: 'Adres e-mail',
                birthdate: 'Data urodzenia',
                year: 'Rok',
                month: 'Miesiąc',
                day: 'Dzień',
                no_set: 'nie podano',
                current_password: 'Aktualne hasło',
                new_password: 'Nowe hasło',
                password: 'Hasło',
                password_confirmation: 'Powtórz hasło',
                no_date: 'brak terminu',
                group: 'Grupa',
                localization: 'Lokalizacja',
                nearest_class_date: 'Najbliższe zajęcia',
                set_password: 'Ustaw hasło',
                login: 'Zaloguj się',
                first_class: 'Rozpoczęcie uczestnictwa',
                has_yet_start: 'Zajęcia jeszcze się nie rozpoczęły, o ich starcie powiadomimy telefonicznie lub e-mailem'
            },

            header: {
                'timetable' : 'Przejdź do grafiku zajęć',
                'save_confirmation' : 'Potwierdzenie zapisu',
                'participant_account' : 'Przejdź do konta uczestnika',
            },

            "cart": {
                "cart": "Zapisy",
                "continue": "Kontynuuj zapisy",
                "checkout": "Przejdź do płatności online",
                topay: "Do zapłaty",
                empty: 'Nie zaleziono żadnych zapisów',
                add: 'Nowy zapis zajęcia',
                add_to: 'Dodaj do koszyka',

            },

            "orders": {
                "orders": "Zamówienia"
            },


            "activities": {
                "activities": "Zajęcia sportowe",
                groups: "Zajęcia grupowe",
            },


            "groups": {
                groups: "Zajęcia grupowe",
                agreement_concluded: 'Zawarta umowa',
                'no_exists': 'Brak uczestnictwa w zajęciach grupowych',
                'new': 'Dokonaj zapisu na zajęcia',
                resignation: 'Rezygnacja z zajęć',
                has_yet_start: 'Nie ustalono',
                no_groups: 'Niestety nie znaleziono zajęć o wybranych parametrach.',
                group_status: {
                    full: 'Grupa pełna',
                    closed: 'Grupa zamknięta',
                    last: 'Ostatnie miejsca',
                },
                procesing_payment: 'Płatność jest przetwarzana, po jej zaksięgowaniu wyślemy powiadomienie wiadomością e-mail.',
                check_payment_status: 'Sprawdź status płatności w panelu klienta',
                rating: 'Jak oceniasz jakość zajęć?',
                reason: 'Powód rezygnacji',
                resignation_action: 'Składam rezygnacje z zajęć',
                resignation_action_termination: 'Składam rezygnacje z zachowaniem okresu wypowiedzenia umowy (:period)',
                player:'Uczestnik zajęć',
                group: 'Grupa',
                actions: {
                    'resign': 'Rezygnacja z zajęć',
                    'reporting_of_absence': 'Zgłoszenie nieobecności',
                    'change_agreement': 'Kontynuacja zajęć z inną umową',

                }
            },


            absense: {
                group: 'Zajęcia',
                player: 'Uczesnik',
                date: 'Okres nieobecności',
                save: 'Wyślij zgłoszenie',
                reason: 'Powód nieobecności',
                end_of_month: 'do końca miesiąca',
                success: 'Dziękujemy. Nieobecność została zgłoszona.',
            },

            "participants": {
                "participants": "Uczestnicy zajęć",
                'no_exists': 'Brak uczestników zajęć',
                'new': 'Dodaj nowego uczestnika',
            },
            "account": {
                "account": "Konto",
                profile: 'Dane konta',
                credit_card: 'Karta płatnicza',
                remove_account: 'Usuwanie konta',
                change_password: 'Zmiana hasła',
                remove_my_account: 'Proszę o usunięcie mojego konta',
                account_removed: 'Konto zostało usunięte'
            },

            agreements: {
                'name_with_price': '{price} - {name}',
                'button': 'Akceptuje  warunki umowy i dokonuje zapisu na zajęcia',
                'free': 'brak odpłatności',
                'calculations': {
                    'one_time_payment':
                        'jednorazowa płatność',
                    'monthly':
                        'płatność miesięczna',
                    'quarterly':
                        'płatność kwartalna',
                    'once_a_season':
                        'płatność jeden raz na sezon',
                    'for_each_class':
                        'płatność za każde zajęcia'

                },
                'is_indefinite': 'nieokreślony',
                'is_without_termination': 'bez okresu wypowiedzenia',
                'unit': {
                    'DAY': 'brak | {count} dzień | {count}  dni',
                    'MONTH': 'brak |  miesiąc | {count} miesiące | {count}  miesięcy',
                    'YEAR': 'brak |  rok | {count} lata | {count}  lat',
                },
                'needRegistartionPay': 'Do powierdzenia zapisu wymgane jest dokonanie płatności {pay}zł',
                'curency': 'PLN'
            },

            loading: 'Wczytuje...',

            description_not_found: 'Nie znaleziono opisu dla tych zajęć',


            'list-of-classes': {
                'filters': {
                    'age-categories': {
                        'name': 'Grupa wiekowa',
                        'all': 'Wszystkie',
                        'format': '{name}'
                    },
                    'sports': {
                        'name': 'Zajęcia',
                        'all': 'Wszystkie',
                        'format': '{name}'
                    },
                    'levels': {
                        'name': 'Poziom zaawansowania',
                        'all': 'Wszystkie',
                        'format': '{name}'
                    },
                    'sections': {
                        'name': 'Lokalizacja',
                        'all': 'Wszystkie',
                        'format': '{name}'
                    },
                    'instructors': {
                        'name': 'Instruktor',
                        'all': 'Wszyscy',
                        'format': '{name} {surname}'
                    },

                },

                'action_button': {

                    'register': 'Zapisy na zajęcia',
                    'register_to_reserve': 'Zapisz się na listę rezerwową',


                },


                group_status: {
                    'full': 'Grupa jest pełna',
                    'closed': 'Grupa zamknięta',
                },

                'table': {
                    'group_name': 'Grupa',
                    'age': 'Wiek uczestnika',
                    'date': 'Dni i godziny',
                    'activity': 'Rodzaj zajęć',
                    'sport': 'Rodzaj zajęć',
                    'level': 'Poziom',
                    'description': 'Opis zajęć',
                    'registration': 'Zapisy',
                    'nearest_class_date': 'Najbliższe zajęcia',
                    'not_started_yet': 'Jeszcze ie rozpoczęto',
                    'no_set_schedule': 'Nie ustalono',
                    'pricing': 'Cennik',
                    'no_set_pricing': '',
                    instructors: 'Instruktor',
                    'age_full': 'od {age_from} do {age_to} lat',
                    'age_from': 'od {age_from} lat',
                    'age_to': 'do {age_to} lat',
                    age_no_limit: 'bez ograniczeń',
                    'no_set_instructor': 'nie podano',


                }


            },

            auth: {
                'login': 'Zaloguj się',
                new_account: 'Załóż nowe konto',
                register: 'Załóż konto'
            },
            auth_page: {
                'login': 'Logowanie',
                no_account: 'Nie masz konta?',
                no_account_desc: 'Konto umożliwia m. in. zapisywanie się online na zajęcia oraz śledzenie stanu płatności w klubie.',
                new: 'Załóż konto',
                register: 'Rejestracja',
                terms: 'Oświadczam, iż zapoznałem się z treścią <a href="/agreements/terms.pdf" target="_blank">Regulaminu</a> aplikacji intenetowej "Panel uczestnika Klubu Samuraj" należącego do Fundacji Samuraj Rozwój Poprzez Sport z siedzibą we Wrocławiu i akceptuję wszystkie jego postanowienia.',
                rodo: 'Administratorem Twoich danych osobowych, czyli podmiotem decydującym o celach i sposobach ich przetwarzania jest Fundacja Samuraj Rozwój Poprzez Sport ul. Bałtycka 19, 51-109 Wrocław. Twoje dane osobowe będą przetwarzane zgodnie z prawem, przede wszystkim z Rozporządzeniem Parlamentu Europejskiego i Rady (UE) o Ochronie Danych Osobowych (RODO), w celu wykonania zawartej z Tobą umowy oraz w celach wskazanych w treści Twoich zgód (Twoje zgody są dobrowolne), przy zachowaniu Twoich praw tj. prawa wycofania zgody na przetwarzanie danych osobowych, prawa dostępu, sprostowania oraz usunięcia Twoich danych, ograniczenia ich przetwarzania, prawa do ich przenoszenia, prawa do niepodlegania zautomatyzowanemu podejmowaniu decyzji, w tym profilowaniu, a także prawa wyrażenia sprzeciwu wobec przetwarzania Twoich danych osobowych (więcej na ten temat w klauzuli informacyjnej). W razie jakichkolwiek Twoich pytań, czy wątpliwości jesteśmy dostępni pod adresem e-mail bok@klubsamuraj.pl. ',
                show_more: 'Zobacz więcej...',
                cut: 'Skróć tekst',
                need: 'Aby dokonać zapisu na zajęcia należy posiadać konto użytkownika. Jeżeli nie posiadsz konta, możesz założyć je bezpłatnie.'
            },
            address: {
                street: 'Ulica i numer',
                city: 'Miasto',
                postal: 'Kod pocztowy',
            },
            payments: {
                created_at: 'Data powstania',
                narration: 'Opis',
                paid: 'Zapłacono',
                pay_should: 'Kwota',
                left_to_pay: 'Pozostało do zapłaty',
                no_exists: 'Jeszcze nie zaksięgowano żadnych płatności',
                amount: 'Kwota',
                payments: 'Płatności',
                payment: 'Płatność',


                pay_all_unpaid: 'Opłać wszystko razem {0} PLN',
                to_pay: 'Do zapłaty',
                pay: 'Opłać',
                history: 'Pokaż historię płatności',
                payment_processed: 'Płatność jest przetwarzana, po jej zaksięgowaniu zostanie wysłane powiadomienie.'
            },



            "my_account": "Moje konto:",
            "ok": "Ok",


            "ok": "Ok",
            "cancel": "Cancel",
            "error_alert_title": "Oops...",
            "error_alert_text": "Something went wrong! Please try again.",
            "token_expired_alert_title": "Session Expired!",
            "token_expired_alert_text": "Please log in again to continue.",
            "login": "Zaloguj się",
            "register": "Register",
            "page_not_found": "Page Not Found",
            "go_home": "Go Home",
            "logout": "Wyloguj się",
            "email": "Adres e-mail",

            "password": "Hasło",
            "forgot_password": "Nie pamiętasz hasła?",


            "confirm_password": "",
            "name": "Name",
            "toggle_navigation": "Toggle navigation",
            "home": "Home",
            "you_are_logged_in": "You are logged in!",
            "reset_password": "Zresetuj hasło",
            "send_password_reset_link": "Wyślij link umożliwjający zmianę hasła",
            "settings": "Settings",
            "profile": "Profile",
            "your_info": "Your Info",
            "info_updated": "Dane zostały zapisane.",
            "update": "Update",
            "your_password": "Your Password",
            "password_updated": "Your password has been updated!",
            "new_password": "New Password",
            "login_with": "Login with",
            "register_with": "Register with",
            "verify_email": "Verify Email",
            "send_verification_link": "Wyślij link umożliwjający zmianę hasła",
            "resend_verification_link": "Resend Verification Link ?",
            "failed_to_verify_email": "Failed to verify email.",
            "verify_email_address": "We sent you an email with an the verification link."
        },
        'en': {


            header: {
                'timetable': 'Go to the timetable',
                'save_confirmation': 'Save confirmation',
                'participant_account': 'Go to participant\'s account',
            },


            'calendar': {
                'day_activity': 'Days of classes:',
                'activity_one': 'Classes on',
                'activity_all': 'Only selected days',
                week: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                weeks: ['Sundays', 'Mondays', 'Tuesdays', 'Wednesdays', 'Thursdays', 'Fridays', 'Saturdays'],

                week_d: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            },


            'form': {
                error: 'Form not saved. Please correct the fields marked in red.',
                save: 'Save',
                edit: 'Change data',
                success: 'Data saved'
            },

            attributes: {
                name: 'Name',
                surname: 'Surname',
                phone: 'Phone number',
                email: 'E-mail address',
                birthdate: 'Birthdate',
                year: 'Year',
                month: 'Month',
                day: 'Day',
                no_set: 'no specified',
                current_password: 'Current password',
                new_password: 'New password',
                'password': 'Password',
                password_confirmation: 'Confirm password',
                no_date: 'no date',
                group: 'Group',
                localization: 'Localization',
                nearest_class_date: 'Nearest class date',
                set_password: 'Set password',
                login: 'Login',
                first_class: 'Starting participation',
                has_yet_start: 'Classes have not started yet, we will notify you about their start by phone or e-mail'
            },

            "cart": {
                "cart": "Registration",
                "continue": "Continue registration",
                "checkout": "Go to online payment",
                topay: "To pay",
                empty: ' No records found',
                add: 'New classes record',


            },

            "orders": {
                "orders": "Orders"
            },


            "activities": {
                "activities": "Sports activities",
                groups: "Group classes",
            },


            "groups": {
                groups: "Group classes",
                'no_exists': 'No participation in group classes',
                'new': 'Register for classes',
                resignation: 'Resignation from classes',
                has_yet_start: 'Not established',
                no_groups: 'No classes with the selected parameters found.',
                group_status: {
                    full: 'Full group',
                    closed: 'Closed group',
                    last: 'Last free places',
                },
                procesing_payment: 'The payment is being processed, we will send you an e-mail notification when it\'s booked.',
                check_payment_status: 'Check payment status in the customer panel',
                rating: 'How do you rate the quality of the classes?',
                reason: 'Reason for resignation',
                resignation_action: 'I submit my resignation from classes',
                resignation_action_termination: 'I submit my resignation with the notice period (: period)',
                player: 'Classes participant',
                group: 'Group',
            },

            "participants": {
                "participants": "Classes participants",
                'no_exists': 'No classes participants',
                'new': 'Add a new participant',
            },
            "account": {
                "account": "Account",
                profile: 'Account details',
                credit_card: 'Payment card',
                remove_account: 'Remove account',
                change_password: 'Change password',
                remove_my_account: 'Please remove my account',
                account_removed: 'Account removed'
            },

            agreements: {
                'name_with_price': '{price} - {name}',
                'button': 'I accept the terms and conditions of the agreement and register for classes',
                'free': 'free',
                'calculations': {
                    'one_time_payment':
                        'one time payment',
                    'monthly':
                        'monthly payment',
                    'quarterly':
                        'quarterly payment',
                    'once_a_season':
                        'payment once a season',
                    'for_each_class':
                        'payment for each class'

                },
                'is_indefinite': 'indefinite',
                'is_without_termination': 'without notice period',
                'unit': {
                    'DAY': 'missing | {count} day | {count} days',
                    'MONTH': 'missing | month | {count} months | {count} months',
                    'YEAR': 'missing | year | {count} years | {count} years',
                },
                'needRegistartionPay': 'Payment of PLN {pay} necessary to confirm the registration',
                'currency': 'PLN'
            },

            loading: 'Loading ...',

            description_not_found: 'No description found for the classes',


            'list-of-classes': {
                'filters': {
                    'age-categories': {
                        'name': 'Age group',
                        'all': 'All',
                        'format': '{name}'
                    },
                    'sports': {
                        'name': 'Classes',
                        'all': 'All',
                        'format': '{name}'
                    },
                    'levels': {
                        'name': 'Level of advancement',
                        'all': 'All',
                        'format': '{name}'
                    },
                    'sections': {
                        'name': 'Localization',
                        'all': 'All',
                        'format': '{name}'
                    },
                    'instructors': {
                        'name': 'Instructor',
                        'all': 'All',
                        'format': '{name} {surname}'
                    },

                },

                'action_button': {

                    'register': 'Registration for classes',
                    'register_to_reserve': 'Registration to the reserve list',


                },


                group_status: {
                    'full': 'The group is full',
                    'closed': 'Group closed',
                },

                'table': {
                    'group_name': 'Group',
                    'age': 'Participant age',
                    'date': 'Days and hours',
                    'activity': 'Type of activities',
                    'sport': 'Type of activities',
                    'level': 'Level',
                    'description': 'Classes description',
                    'registration': 'Registration',
                    'nearest_class_date': 'Nearest class date',
                    'not_started_yet': 'Not started yet',
                    'no_set_schedule': 'Not established',
                    'pricing': 'Price list',
                    'no_set_pricing': '',
                    instructors: 'Instructor',
                    'age_full': 'from {age_from} to {age_to} years',
                    'age_from': 'from {age_from} years',
                    'age_to': 'to {age_to} years',
                    age_no_limit: 'no limits',
                    'no_set_instructor': 'not specified',


                }


            },

            auth: {
                'login': 'Login',
                new_account: 'Create a new account',
                register: 'Create account'
            },
            auth_page: {
                'login': 'Login',
                no_account: 'Not registered?',
                no_account_desc: 'The account allows, among others online registration for classes and tracking the payment status in the club.',
                new: 'Create an account',
                register: 'Registration',
                terms: 'I declare that I have read the <a href="/agreements/terms.pdf" target="_blank">Regulations</a> of the "Samurai Club Participant Panel" Internet application belonging to Fundacja Samuraj Rozwój Poprzez Sport [the Samurai Development Through Sport Foundation] based in Wrocław, and I accept all its provisions.',
                GDPR: 'The controller of your personal data, i.e. the entity deciding about the purposes and methods of the data processing, is Fundacja Samuraj Rozwój Poprzez Sport, ul. Bałtycka 19, 51-109 Wrocław. Your personal data will be processed in accordance with the law, in particular in accordance with the Regulation of the European Parliament and of the Council (EU) on the Personal Data Protection (GDPR), in order to perform the agreement concluded with you and for the purposes indicated in the content of your consents (your consents are optional), while maintaining your rights, i.e. the right to withdraw your consent to the processing of personal data, the right to access, rectify and delete your data, limit its processing, the right to transfer it, the right not to be subject to automated decision making, including profiling, and the right to object to processing your personal data (more can be found in the information clause). If you have any questions or doubts, we are available at the email address bok@klubsamuraj.pl. ',
                show_more: 'See more ...',
                cut: 'Shorten Text',
                need: 'To register for classes, you must have a user account. If you don\'t have an account, you can create it free.'
            },
            address: {
                street: 'Street and number',
                city: 'City',
                postal: 'Postal code',
            },
            payments: {
                created_at: 'Date of creation',
                narration: 'Description',
                paid: 'Paid',
                pay_should: 'Amount',
                left_to_pay: 'Left to pay',
                no_exists: 'No payments have been booked yet',
                'amount': 'Amount',
                "payments": "Payments",
                "payment": "Payments",

                pay_all_unpaid: 'Pay the payment {0} PLN',
                to_pay: 'To be paid',
                pay: 'Pay',
                history: 'Show payment history',
                payment_processed: 'Thank you for using online payment. After the transaction is posted, a notification will be sent. '
            },



            "my_account": "My account:",
            "ok": "Ok",


            "cancel": "Cancel",
            "error_alert_title": "An error occurred",
            "error_alert_text": "Please try again later.",
            "token_expired_alert_title": "Your session has expired.",
            "token_expired_alert_text": "Please login again to continue",
            "login": "Log in",
            "register": "Register",
            "page_not_found": "The page you are looking for does not exist",
            "go_home": "Go to registration",
            "logout": "Log out",
            "email": "E-mail address",

            "password": "Password",
            "forgot_password": "Forgot your password?",


            "confirm_password": "Confirm password",

            "reset_password": "Reset password",
            "send_password_reset_link": "Send a link to change the password",
            "settings": "Settings",
            "profile": "Profile",
            "info_updated": "Data saved.",
            "new_password": "New password",

            "resend_verification_link": "Resend the link",
            "failed_to_verify_email": "Failed to verify email.",
            "verify_email_address": "E-mail with link has been sent"
        },
        'ukr': {


            header: {
                'timetable': 'Перейти до розкладу',
                'save_confirmation': 'Зберегти підтвердження',
                'participant_account': 'Перейти на рахунок учасника',
            },

            'calendar': {
                'day_activity': 'Дні занять:',
                'activity_one': 'Заняття (даного) дня',
                'activity_all': 'Тільки вибрані дні',
                week: ['неділя', 'понеділок', 'вівторок', 'середа', 'четвер', 'п\'ятниця', 'субота'],
                weeks: ['неділі', 'понеділки', 'вівторки', 'середи', 'четверги', 'п\'ятниці', 'суботи'],

                weeks_d: ['неділя', 'понеділок', 'вівторок', 'середа', 'четвер', 'п\'ятниця', 'субота'],
                months: ["січень", "лютий", "березень", "квітень", "травень", "серпень", "вересень", "жовтень", "листопад", "грудень"],
            },


            'form': {
                error: 'Не записано формуляр. Прошу виправити рубрики, позначені червоним.',
                save: 'Записати',
                edit: 'Змінити дані',
                success: 'Дані записані'
            },

            attributes: {
                name: 'Ім\'я',
                surname: 'Прізвище',
                phone: 'Номер телефону',
                email: 'Адреса електронної пошти',
                birthdate: 'Дата народження',
                year: 'Рік',
                month: 'Місяць',
                day: 'День',
                no_set: 'не вказано',
                current_password: 'Актуальний пароль',
                new_password: 'Новий пароль',
                password: 'Пароль',
                password_confirmation: 'Повтори пароль',
                no_date: 'немає терміну',
                group: 'Група',
                localization: 'Локалізація',
                nearest_class_date: 'Найближчі заняття',
                set_password: 'Вибери пароль',
                login: 'Увійти',
                first_class: 'Початок участі',
                has_yet_start: 'Заняття ще не почалися, про їх початок ми повідомимо по телефону або електронною поштою'
            },

            "cart": {
                "cart": "Записи",
                "continue": "Продовжуй записи",
                "checkout": "Перейди до оплати онлайн",
                topay: "До оплати",
                empty: 'Не знайдено записів',
                add: 'Новий запис заняття'
            },

            "orders": {
                "orders": "Замовлення"
            },


            "activities": {
                "activities": "Спортивні заняття",
                groups: "Групові заняття"
            },


            "groups": {
                groups: "Групові заняття",
                'no_exists': 'Без участі в групових заняттях',
                'new': 'Запишися на заняття',
                resignation: 'Відмова від занять',
                has_yet_start: 'Не встановлено',
                no_groups: 'На жаль не знайдено занять з запитуваними параметрами.',
                group_status: {
                    full: 'Група повна',
                    closed: 'Група закрита',
                    last: 'Останні місця',
                },
                procesing_payment: 'Платіж обробляється, після його підтвердження ми надішлемо повідомлення електронною поштою.',
                check_payment_status: 'Перевір статус оплати в панелі клієнта',
                rating: 'Як ти оцінюєш якість занять?',
                reason: 'Причини відмови',
                resignation_action: 'Подаю відмову від занять',
                resignation_action_termination: 'Подаю відмову з дотриманням періоду попередження про припинення договору (:period)',
                player:'Учасник занять',
                group: 'Група',
            },

            "participants": {
                "participants": "Учасники занять",
                'no_exists': 'Немає учасників занять',
                'new': 'Додати нового учасника'
            },
            "account": {
                "account": "Профіль",
                profile: 'Дані профіля',
                credit_card: 'Платіжна карта',
                remove_account: 'Усування профіля',
                change_password: 'Зміна паролю',
                remove_my_account: 'Прошу усунути мій профіль',
                account_removed: 'Профіль усунено'
            },

            agreements: {
                'name_with_price': '{price} - {name}',
                'button': 'Приймаю умови договору і записуюсь на заняття',
                'free': 'без оплати ',
                'calculations': {
                    'one_time_payment':
                        'одноразова оплата',
                    'monthly':
                        'місячна оплата',
                    'quarterly':
                        'квартальна оплата',
                    'once_a_season':
                        'оплата раз в сезоні',
                    'for_each_class':
                        'оплата за кожні заняття'

                },
                'is_indefinite': 'без терміново',
                'is_without_termination': 'без періоду попередження про припинення',
                'unit': {
                    'DAY': 'немає | {count} день | {count}  дні',
                    'MONTH': 'немає |  місяць | {count} місяці | {count}  місяців',
                    'YEAR': 'немає |  рік | {count} роки | {count}  років',
                },
                'needRegistartionPay': 'До передачі запису вимагається здійснення оплати {pay}злотих',
                'curency': 'PLN'
            },

            loading: 'Ввід...',

            description_not_found: 'Не знайдено опису для цих занять',


            'list-of-classes': {
                'filters': {
                    'age-categories': {
                        'name': 'Вікова група',
                        'all': 'Всі',
                        'формат': '{name}'
                    },
                    'sports': {
                        'name': 'Заняття',
                        'all': 'Всі',
                        'формат': '{name}'
                    },
                    'levels': {
                        'name': 'Рівень складності',
                        'all': 'Всі',
                        'формат': '{name}'
                    },
                    'sections': {
                        'name': 'Локалізація',
                        'all': 'Всі',
                        'формат': '{name}'
                    },
                    'instructors': {
                        'name': 'Інструктор',
                        'all': 'Всі',
                        'формат': '{name} {surname}'
                    },

                },

                'action_button': {

                    'register': 'Записи на заняття',
                    'register_to_reserve': 'Запишись в резервний список',


                },


                group_status: {
                    'full': 'Група повна',
                    'closed': 'Група закрита',
                },

                'table': {
                    'group_name': 'Група',
                    'age': 'Вік учасника',
                    'date': 'Дні і години',
                    'activity': 'Вид занять',
                    'sport': 'Вид занять',
                    'level': 'Рівень',
                    'description': 'Опис занять',
                    'registration': 'Записи',
                    'nearest_class_date': 'Найближчі заняття',
                    'not_started_yet': 'Ще не почалися',
                    'no_set_schedule': 'Не встановлено',
                    'pricing': 'Прейскурант',
                    'no_set_pricing': '',
                    instructors: 'Інструктор',
                    'age_full': 'з {age_from} до {age_to} років',
                    'age_from': 'з {age_from} років',
                    'age_to': 'до {age_to} років',
                    age_no_limit: 'без обмежень',
                    'no_set_instructor': 'не вказано',


                }


            },

            auth: {
                login: 'Увійти',
                new_account: 'Створити новий профіль',
                register: 'Утворити профіль'
            },
            auth_page: {
                login: 'Вхід',
                no_account: 'У Вас немає профілю?',
                no_account_desc: 'Профіль дозволяє, серед іншого записуватися онлайн на заняття та слідкувати за станом оплати в клубі.',
                new: 'Утворити профіль',
                register: 'Реєстрація',
                terms: 'Підтверджую ознайомлення зі змістом <a href="/agreements/terms.pdf" target="_blank">Regulaminu</a> застосунку "Панель учасника "Клубу Самурай", члена фундації "Самурай - розвиток за допомогою спорту" з місцезнаходженням у Вроцлаві і приймаю всі його положення.',
                GDPR: 'Адміністратор Твоїх персональних даних, тобто суб\'єкт, який приймає рішення про цілі і способи їх використання - фундація "Самурай - розвиток за допомогою спорту" вул. Bałtycka 19, 51-109 Вроцлав. Твої персональні дані використовуватимуться відповідно до законодавства, перш за все Регламенту Європарламенту і Ради (ЄС) про захист персональних даних (GDPR), з метою виконання укладеного з Вами договору та в цілях, вказаних в змісті Твої згод (Твоя згода добровільна), при дотриманні Твоїх прав, тобто права анулювання згоди на використання персональних даних, права доступу, спростування та усунення Твоїх даних, обмеження їх використання, права їх перенесення, права на не підлягання механізованому прийняттю рішення, в цій профілізації, а також права заперечення проти використання Твоїх персональних даних (детальніше про це в інформаційній клаузулі). У випадку виникнення будь-яких питань, чи сумнівів ми доступні за адресою електронної пошти bok@klubsamuraj.pl. ',
                show_more: 'Детальніше...',
                cut: 'Скоротити текст',
                need: 'Щоб записатися на заняття потрібно мати профіль користувача. Якщо не маєш рахунку, можеш утворити його безкоштовно.'
            },
            address: {
                street: 'Вулиця і номер',
                city: 'Місто',
                postal: 'Індекс',
            },
            payments: {
                created_at: 'Дата створення',
                narration: 'Опис',
                paid: 'Оплачено',
                pay_should: 'Сума',
                left_to_pay: 'Залишилося до оплати',
                no_exists: 'Ще не надійшло жодних оплат',
                amount: 'Сума',
                "payments": "Оплати",
                "payment": "Оплати",

                pay_all_unpaid: 'Оплатити платіж {0} PLN',
                to_pay: 'До оплати',
                pay: 'Оплатити',
                history: 'Показати історію платежів',
                payment_processed: 'Дякуємо за використання онлайн-платежів. Після опублікування транзакції буде надіслано повідомлення. '
            },



            "my_account": "Мій профіль:",
            "ok": "Ок",


            "cancel": "Анулюй",
            "error_alert_title": "Виникла помилка",
            "error_alert_text": "Спробуй пізніше.",
            "token_expired_alert_title": "Ваша сесія закінчилася.",
            "token_expired_alert_text": "Щоб продовжити увійди повторно",
            "login": "Вхід",
            "register": "Register",
            "page_not_found": "Сторінка яку ти шукаєш не існує",
            "go_home": "Перейти до записів",
            "logout": "Вихід",
            "email": "Електронна адреса",

            "password": "Пароль",
            "forgot_password": "Не пам'ятаєш пароль?",
            "confirm_password": "Повтори пароль",

            "reset_password": "Зареєструй пароль",
            "send_password_reset_link": "Відправ посилання для зміни паролю",
            "settings": "Налаштування",
            "profile": "Профіль",
            "info_updated": "Дані записані.",
            "new_password": "Новий пароль",

            "resend_verification_link": "Відправ посилання повторно",
            "failed_to_verify_email": "Failed to verify email.",
            "verify_email_address": "Мейл з посиланням надіслано"
        }
    }
})

// /**
//  * @param {String} locale
//  */
// export async function loadMessages(setlocale) {
//
//
//     if(!setlocale) {
//
//         let locale = Cookies.get('locale');
//
//         if (!locale) {
//
//             let locale = store.getters['lang/locale']
//
//             if (!locale) {
//
//                 var lang = window.navigator.userLanguage || window.navigator.language;
//
//                 if (lang) {
//
//                     let pref = lang.substring(0, 2);
//
//                     if (['pl','en','ukr'].includes(pref))
//                         locale = pref;
//
//                 }
//
//
//
//
//             }
//
//         }
//
//         setlocale = locale || 'pl';
//     }
//
//
//     i18n.locale = setlocale ;
//
//
//     // if (Object.keys(i18n.getLocaleMessage(locale)).length === 0) {
//     //  const messages = await import(/* webpackChunkName: '' */ `../lang/${locale}`)
//     //  i18n.setLocaleMessage(locale, messages)
//     // }
//
//     // if (i18n.locale !== locale) {
//     //   = locale
//     // }
// }
// //

//
export default i18n;
