import axios from "axios";
//import VueAxios from "vue-axios";
import { API_URL } from "@/config";

// Request interceptor
axios.interceptors.request.use(request => {
  // const token = store.getters['auth/token']
  //
  // if (token) {
  //   request.headers.common['Authorization'] = `Bearer ${token}`
  // }
  //
  // const locale = store.getters['lang/locale']
  // if (locale) {
  //   request.headers.common['Accept-Language'] = locale
  // }
  //
  //   const srf_token = Cookies.get('XSRF-TOKEN')
  //
  //   console.log(srf_token);
  //
  //   if (srf_token) {
  //       request.headers.common['X-CSRF-TOKEN'] = srf_token
  //   }
  //
  //
  // // request.headers['X-Socket-Id'] = Echo.socketId()

  return request
})

// Response interceptor
axios.interceptors.response.use(response => response, error => {
 // const { status } = error.response
console.log(error);
  // if (status >= 500) {
  //   Swal.fire({
  //     type: 'error',
  //     title: i18n.t('error_alert_title'),
  //     text: i18n.t('error_alert_text'),
  //     reverseButtons: true,
  //     confirmButtonText: i18n.t('ok'),
  //     cancelButtonText: i18n.t('cancel')
  //   })
  // }
  //
  // if (status === 401 && store.getters['auth/check']) {
  //   Swal.fire({
  //     type: 'warning',
  //     title: i18n.t('token_expired_alert_title'),
  //     text: i18n.t('token_expired_alert_text'),
  //     reverseButtons: true,
  //     confirmButtonText: i18n.t('ok'),
  //     cancelButtonText: i18n.t('cancel')
  //   }).then(() => {
  //     store.commit('auth/LOGOUT')
  //
  //     router.push({ name: 'login' })
  //   })
  // }

  return Promise.reject(error)
})

axios.defaults.baseURL = API_URL;

export default axios;