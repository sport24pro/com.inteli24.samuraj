//import Vue from "vue";
import axios from "@/plugins/axios";

// import JwtService from "@/common/jwt.service";
import { API_URL } from "@/config";

const ApiService = {
  init() {
    // Vue.use(VueAxios, axios);
    // Vue.axios.defaults.baseURL = API_URL;
  },

  // setHeader() {
  //   Vue.axios.defaults.headers.common[
  //     "Authorization"
  //   ] = `Token ${JwtService.getToken()}`;
  // },

  query(resource, params) {
    return axios.get(resource, params);
  },

  get(resource, slug = "") {
    return axios.get(`${resource}/${slug}`);
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  }
};

export default ApiService;

export const GroupsService = {
  all() {
    return ApiService.get("available-activities-with-filters");
  }
};

export const SettingService = {
  all() {
    return ApiService.get("get-service-channel-configuration",'MOBILE_APP');
  }
};


export const PageService = {
  show(slug) {
    return ApiService.get("pages",slug);
  }
};


export const ProductsService = {
  all() {
    return ApiService.get("products",'MOBILE_APP');
  }
};


export const EventsService = {
  all() {
    return ApiService.get("events",'MOBILE_APP');
  }
};



export const ArticlesService = {
  query(type, params) {
    return ApiService.query("articles" + (type === "feed" ? "/feed" : ""), {
      params: params
    });
  },
  get(slug) {
    return ApiService.get("articles", slug);
  },
  create(params) {
    return ApiService.post("articles", { article: params });
  },
  update(slug, params) {
    return ApiService.update("articles", slug, { article: params });
  },
  destroy(slug) {
    return ApiService.delete(`articles/${slug}`);
  }
};

// export const CommentsService = {
//   get(slug) {
//     if (typeof slug !== "string") {
//       throw new Error(
//         "[RWV] CommentsService.get() article slug required to fetch comments"
//       );
//     }
//     return ApiService.get("articles", `${slug}/comments`);
//   },
//
//   post(slug, payload) {
//     return ApiService.post(`articles/${slug}/comments`, {
//       comment: { body: payload }
//     });
//   },
//
//   destroy(slug, commentId) {
//     return ApiService.delete(`articles/${slug}/comments/${commentId}`);
//   }
// };

