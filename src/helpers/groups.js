
export default  {

    instructors: (group,data) => {


        if(!_.size(group.instructors))
            return 'list-of-classes.table.no_set_instructor';


        let instructors='';

        _.forEach(group.instructors, (v) => {

            instructors+='<div class="instructor_name">' +_.get(data,'instructors.' + v.id+'.name','')+' '+_.get(data,'instructors.' + v.id+'.surname','')+'</div>';
            //       instructors+='<span class="instructor">' +_.get(data,'filters.instructors.' + v.id+'.name','')+' '+_.get(data,'filters.instructors.' + v.id+'.surname','')+'</span>';

        });

        return instructors;


    },


    pricing:  (group,data) => {


        if(_.size(group.agreements)) {

            let pricing='';




            _.forEach(group.agreements, (v) => {

                let pay = _.head( _.get(data,'agreements'+v.id+'.pay'));

                if(!_.isEmpty(pay)) {
                    pricing+='ssss';
                    // pricing += '<div class="pricing">' + i18n.t('agreements.name_with_price', {
                    //     name: pay.name,
                    //     price: pay.payment
                    // }) + '</div>';
                }
            });

            return pricing;



        }



        if (_.size(data.agreements)) {
            let pricing='';

            _.forEach(data.agreements, (v) => {
                let pay = _.head(v.pay);

                if(!_.isEmpty(pay)) {
                    pricing+='ssss';
                    // pricing += '<div class="pricing">' + i18n.t('agreements.name_with_price', {
                    //     name: pay.name,
                    //     price: pay.payment
                    // }) + '</div>';
                }
            });

            return pricing;

        }


        return 'list-of-classes.table.no_set_pricing';

    },

    nearest_class_date: (nearest_class_date,has_yet_start)=>  {

        if(has_yet_start)
            return i18n.t('groups.has_yet_start');

        if(nearest_class_date) {

            let t = nearest_class_date.split(/[- :T\.]/);


            return new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], 0))
                .toLocaleDateString('pl', {
                    hour: 'numeric',
                    minute: '2-digit',
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    timeZone: 'UTC',
                });


        }
        return '';

    },

    age: (group) => {



        if(group.age_from &&  group.age_to)
            return 'list-of-classes.table.age_full';


        if(group.age_from)
            return 'list-of-classes.table.age_from';


        if(group.age_to)
            return 'list-of-classes.table.age_to';

        return 'list-of-classes.table.age_no_limit';
    },

    actvity_date: (group) => {



        if(_.isEmpty(group.group_schedule))
            return 'list-of-classes.table.no_set_schedule';

        let actvity_date = '';

        _.forEach(group.group_schedule, (v) => {
            actvity_date+= 'ddd';
            //actvity_date += '<div class="actvity_date">' + i18n.t('calendar.weeks.' + v.period) + ' ' + v.start.substring(0, 5) + ' - ' + v.end.substring(0, 5)+'</div>';
        });


        return actvity_date;
    },



}
