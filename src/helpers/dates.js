export default  {

    format: (date_iso_string) => {

        if(!date_iso_string)
            return null;


        let t = date_iso_string.split(/[- :T\.]/);


        return new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]))
            .toLocaleDateString('pl', {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                timeZone: 'UTC',
            })


    },

    formatFull: (date_iso_string) => {

        if(!date_iso_string)
            return null;

        let t = date_iso_string.split(/[- :T\.]/);

        return new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]))
            .toLocaleDateString('pl', {
                     hour: 'numeric',
                    minute: '2-digit',
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                timeZone: 'UTC',
            })


    },
}

