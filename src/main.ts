import {createApp} from 'vue'
import App from './App.vue'
import router from './router';

import {IonicVue} from '@ionic/vue';


import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

//import '@tailwindcss/postcss7-compat/dist/tailwind-dark.css';

// import '@/theme/tailwind.css';
import '@/theme/variables.css';
import '@/theme/style.css';

import {store} from "./store";


import ApiService from "@/helpers/api.service";

ApiService.init();


import i18n from '@/plugins/i18n.js'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
import GoogleMap from 'googlemaps-vue3'

const app = createApp(App)
    .use(IonicVue)
    .use(router)
    .use(store)
    .use(VueLodash)
    .use(GoogleMap, {apiKey: 'AIzaSyCTs8x-2RXumipG9zwjBLc50Xxt9IE0zGo'})
    .use(i18n)
//
;

app.config.globalProperties.$_ = lodash;


router.isReady().then(() => {
    app.mount('#app');
});