import axios from 'axios'
import * as types from '../mutation-types'

import groupHelper from '../../helpers/groups.js';

// state
export const state = {
    cart: {
        items: {},
        availableShippingMethods: {},
        coupons: {},
        discounts: {},
        quantity: 0,
        shippingCost: 0,
        subTotal: 0,
        taxes: 0,
        total: {
            amount: 0,
            currency: "",
            formatted: '',
            inCurrentCurrency: {}
        }
    },
    openSidebar: false,
}

// getters
export const getters = {
    cart: state => state.cart,
    items: state => state.cart.items,
    cartIsEmpty: state => (_.size(state.cart.items) == 0),
    isOpenSidebar: state => state.openSidebar,

}


// mutations
export const mutations = {

    [types.ADD_CART_ITEM](state, {cartItem}) {
        state.items[cartItem.id] = cartItem;
    },

    [types.UPDATE_CART](state, cart ) {
        state.cart = cart;
    },


    [types.REMOVE_CART_ITEM](state, cartItem) {

            Vue.delete(state.cart.items, cartItem.id);

    },

    [types.CLEAR_CART](state) {
        state.items = {};
    },

    [types.OPEN_CART_SIDEBAR](state) {
        state.openSidebar =  true;
    },


    [types.CLOSE_CART_SIDEBAR](state) {
        state.openSidebar = false;
    },
}

// actions
export const actions = {

    removeFromCart ({ commit }, payload) {
        commit(types.REMOVE_CART_ITEM, payload)
    },

    addToCart ({ commit }, payload) {
        commit(types.ADD_CART_ITEM, payload)
    },

      updateCart ({ commit }, payload) {
        commit(types.UPDATE_CART, payload)

          if(_.size(payload.items))
              commit(types.OPEN_CART_SIDEBAR)
          else
              commit(types.CLOSE_CART_SIDEBAR)

    },

   async  clearCart ({ commit }) {

        axios.post(route('cart.clear.store'));

        commit(types.CLEAR_CART)
    },
    openSidebar ({ commit }) {
        commit(types.OPEN_CART_SIDEBAR)
    },
    closeSidebar ({ commit }) {
        commit(types.CLOSE_CART_SIDEBAR)
    },
}
