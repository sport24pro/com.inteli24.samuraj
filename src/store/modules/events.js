

import {
    EventsService
} from "@/helpers/api.service";

import * as types from '../mutation-types'

import groupHelper from '../../helpers/groups.js';

// state
export const state = {
    data: {},
    events: {},
    exams: {},
    isLoading: null,
    resing_group: {}
}

// getters
export const getters = {
    events: state => state.data,
    isLoading: state => state.isLoading,
    eventsLoaded: state => _.size(state.data),

    getSectionByGroupId: (state) => (id) => {

        let gr = null;

        id =parseInt(id);

        _.forEach(state.data.sections, (section,s) => {

            _.forEach(section.groups, (group,g) => {

                if(group.id == id) {
                    gr = state.data.sections[s].groups[g];
                    return false;
                }

            });


            if(gr)
                return false;

        });



        return gr;
    },

    getSectionById: (state) => (id) => {

        let sc = null;

        _.forEach(state.data.sections, (section,s) => {

                if(section.id == id) {
                    sc =section;

                }



        });


        return sc;
    },

    getAgreementById: (state) => (id) => {

        return _.find(state.data.agreements, (o) => { return o.id == id },{});
    },

    getAgreementsByGroup: (state) => (id) => {


        let gr = null;
        _.forEach(state.data.sections, (section,s) => {

            _.forEach(section.groups, (group,g) => {
                if(group.id == id) {
                    gr = group;
                    return false;
                }

            });



        });

        if(!gr)
            return {};



        let agreements_ids = _.flatMap(gr.agreements, (n) => { return n.id; });

        let agreements = [];

        _.forEach(agreements_ids, (r) => {

            if(state.data.agreements[r]) {
                agreements.push(state.data.agreements[r]);
            }
        });

        return agreements
    },



}


// mutations
export const mutations = {
    [types.SAVE_EVENTS](state, {data}) {

        state.isLoading = false;
        
        _.forEach(data.products, (product,s) => {

            data.products[s].ex = _.get(data,'products_ex.'+product.id,{});


        });

        state.data = Object.freeze( data.products );

        state.isLoading = false;
    },

    [types.ADD_TO_GROUP](state, {group}) {
        state.groups[group.id] = group;
    },

}

// actions
export const actions = {

    async fetchEvents({commit}) {

        // dane są załadowene lub wczytywane

        if(_.size(state.data) || state.isLoading)
            return false;

        state.isLoading = true;



   await  EventsService.all()
         .then(function (response) {
                        commit(types.SAVE_EVENTS, {data: response.data} )
                });



    },

     resingGroup({commit},payload) {


           commit(types.RESIGN_GROUP, {data: response.data} )


    },

}
