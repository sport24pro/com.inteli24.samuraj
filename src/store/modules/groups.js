

import {
    GroupsService
} from "@/helpers/api.service";

import * as types from '../mutation-types'

import groupHelper from '../../helpers/groups.js';

// state
export const state = {
    data: {},
    events: {},
    exams: {},
    isLoading: null,
    resing_group: {}
}

// getters
export const getters = {
    data: state => state.data,
    isLoading: state => state.isLoading,
    srf_token: state => state.srf_token,
    exists: state => _.size(state.data),

    getSectionByGroupId: (state) => (id) => {

        let gr = null;

        id =parseInt(id);

        _.forEach(state.data.sections, (section,s) => {

            _.forEach(section.groups, (group,g) => {

                if(group.id == id) {
                    gr = state.data.sections[s].groups[g];
                    return false;
                }

            });


            if(gr)
                return false;

        });



        return gr;
    },

    getSectionById: (state) => (id) => {

        let sc = null;

        _.forEach(state.data.sections, (section,s) => {

                if(section.id == id) {
                    sc =section;

                }



        });


        return sc;
    },

    getAgreementById: (state) => (id) => {

        return _.find(state.data.agreements, (o) => { return o.id == id },{});
    },

    getAgreementsByGroup: (state) => (id) => {


        let gr = null;
        _.forEach(state.data.sections, (section,s) => {

            _.forEach(section.groups, (group,g) => {
                if(group.id == id) {
                    gr = group;
                    return false;
                }

            });



        });

        if(!gr)
            return {};



        let agreements_ids = _.flatMap(gr.agreements, (n) => { return n.id; });

        let agreements = [];

        _.forEach(agreements_ids, (r) => {

            if(state.data.agreements[r]) {
                agreements.push(state.data.agreements[r]);
            }
        });

        return agreements
    },



}


// mutations
export const mutations = {
    [types.SAVE_GROUPS](state, {data}) {

        state.isLoading = false;
        
        _.forEach(data.groups.sections, (section,s) => {

            _.forEach(section.groups, (group,g) => {

                data.groups.sections[s].id = parseInt(section.id);
                data.groups.sections[s].groups[g].id = parseInt(group.id);
                data.groups.sections[s].groups[g].parsed_age = groupHelper.age(group);

                data.groups.sections[s].groups[g].parsed_pricing = groupHelper.pricing(group,data.groups);
                data.groups.sections[s].groups[g].parsed_actvity_date = groupHelper.actvity_date(group);
                data.groups.sections[s].groups[g].parsed_instructors = groupHelper.instructors(group,data.groups);


                data.groups.sections[s].groups[g].ex = _.get(data.groups_ex,'data.groups_ex.'+group.id,{});

                data.groups.sections[s].groups[g].parsed_nearest_class_date = groupHelper.nearest_class_date(
                    _.get(data.groups_ex,'data.groups_ex.'+group.id+'.nearest_class_date',''),
                        _.get(data.groups_ex,'data.groups_ex.'+group.id+'+.has_yet_start','')) ;

                group.parsed_sport = _.get(data.groups,'filters.sports.' + group.sport_id+'.name','');
                group.parsed_level = _.get(data.groups,'filters.levels.' + group.level_id+'.name','');


            });



        });

        state.data = Object.freeze( data.groups );

        console.log(data.groups );

        // remove all groups description
        if(typeof window.sessionStorage.clear == "function") {
            window.sessionStorage.clear();
        }
    },

    [types.ADD_TO_GROUP](state, {group}) {
        state.groups[group.id] = group;
    },

}

// actions
export const actions = {

    async fetchGroups({commit}) {


   await  GroupsService.all()
         .then(function (response) {
                        commit(types.SAVE_GROUPS, {data: response.data} )
                });



    },

     resingGroup({commit},payload) {


           commit(types.RESIGN_GROUP, {data: response.data} )


    },

}
