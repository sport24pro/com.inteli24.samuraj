

import {
    SettingService
} from "@/helpers/api.service";

import * as types from '../mutation-types'

// state
export const state = {
    setting: {},
    isLoaded: null,
}

// getters
export const getters = {
    setting: state => state.setting,
    isLoaded: state => state.isLoaded,

    primaryTabs: state => _.get(state.setting,'mobile_detail.tabs_menu.menu_items',[]),
    primaryMenu: state => _.get(state.setting,'primary_menu.menu_items',[]),

    club: state => _.get(state.setting,'club',{}),
    logo: state => _.get(state.setting,'club.base_logo.0.path',[])
}


// mutations
export const mutations = {
    [types.SAVE_SETTING](state, {data}) {

        state.isLoaded = Date.now();

        state.setting = Object.freeze( data );

    //    console.log(  state.setting, ':O');

    },

}

import { loadingController } from '@ionic/vue';


// actions
export const actions = {

    async fetchSetting({commit}) {

        const loading = await loadingController
            .create({
                cssClass: 'my-custom-class',
                message: 'Wczytuję',
                duration: -1,
            });

         loading.present();

   await  SettingService.all()
         .then(function (response) {
             loading.dismiss()
                        commit(types.SAVE_SETTING, {data: response.data} )
                });



    },

     resingGroup({commit},payload) {


           commit(types.RESIGN_GROUP, {data: response.data} )


    },

}
