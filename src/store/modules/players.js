import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  players: {},
}

// getters
export const getters = {
  players: state => state.players,
  exists: state => _.size(state.players)
}



// mutations
export const mutations = {
  [types.SAVE_PLAYERS] (state, { players }) {
    state.players = _.keyBy(players, 'id');
  },

  [types.UPDATE_PLAYER] (state, { player }) {
      state.players[player.id] = player;
  },

}

// actions
export const actions = {
  savePlayer ({ commit, dispatch }, payload) {
    commit(types.SAVE_PLAYERS, payload)
  },

  async fetchPlayers ({ commit }) {
    try {
      const { data } = await axios.get( route('api.user.players.index') )

      commit(types.SAVE_PLAYERS, { players: data.data })
    } catch (e) {

    }
  },

  updatePlayer ({ commit }, payload) {
    commit(types.UPDATE_PLAYER, payload)
  },

  async logout ({ commit }) {
    try {

      await axios.post( route('api.user.logout') )
          .then(function (response) {
          commit(types.LOGOUT);
        });


    } catch (e) { }


  },

  async fetchOauthUrl (ctx, { provider }) {
    const { data } = await axios.post( route( 'api.user.oauth',{ driver: provider }))

    return data.url
  }
}
