import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');

import * as types from '../mutation-types'

import groupHelper from '../../helpers/groups.js';

// state
export const state = {
    channel_listening: false,
    echo: new Echo({
        broadcaster: 'pusher',
        key: '50809da4ebe381b28dd4',
        cluster: 'eu',
        forceTLS: true
    }),
    notifiable_public_channels: [
        {
            channel: 'Notificacio',
            event: 'Notificacio'
        },
        {
            channel: 'EstatRepetidor',
            event: 'BroadcastEstatRepetidor'
        }
    ]
}

// getters
export const getters = {



}


// mutations
export const mutations = {
    // [types.SAVE_GROUPS](state, from_ajax) {
    //
    //     state.isLoading = false;
    //
    //     let data = window.appdata;
    //
    //     _.forEach(data.sections, (section,s) => {
    //
    //         _.forEach(section.groups, (group,g) => {
    //
    //             data.sections[s].id = parseInt(section.id);
    //             data.sections[s].groups[g].id = parseInt(group.id);
    //             data.sections[s].groups[g].parsed_age = groupHelper.age(group);
    //
    //             data.sections[s].groups[g].parsed_pricing = groupHelper.pricing(group,data);
    //             data.sections[s].groups[g].parsed_actvity_date = groupHelper.actvity_date(group);
    //             data.sections[s].groups[g].parsed_instructors = groupHelper.instructors(group,data);
    //
    //
    //             data.sections[s].groups[g].ex = _.get(from_ajax,'data.groups_ex.'+group.id,{});
    //
    //             data.sections[s].groups[g].parsed_nearest_class_date = groupHelper.nearest_class_date(
    //                 _.get(from_ajax,'data.groups_ex.'+group.id+'.nearest_class_date',''),
    //                     _.get(from_ajax,'data.groups_ex.'+group.id+'+.has_yet_start','')) ;
    //
    //             group.parsed_sport = _.get(data,'filters.sports.' + group.sport_id+'.name','');
    //             group.parsed_level = _.get(data,'filters.levels.' + group.level_id+'.name','');
    //
    //
    //         });
    //
    //
    //
    //     });
    //
    //     state.data = Object.freeze( data );
    //
    //     // remove all groups description
    //     if(typeof window.sessionStorage.clear == "function") {
    //         window.sessionStorage.clear();
    //     }
    // },
    //
    // [types.ADD_TO_GROUP](state, {group}) {
    //     state.groups[group.id] = group;
    // },

}

// actions
export const actions = {

   initChannels (context) {
        console.log('Initializing channel listeners...')
        context.commit('SET_CHANNEL_LISTENING', true)

        context.getters.notifiable_public_channels.forEach(listener => {
            context.dispatch(actions.INIT_PUBLIC_NOTIFIABLE_CHANNEL_LISTENER, listener)
        })
        // }
    },

    [ 'INIT_PUBLIC_NOTIFIABLE_CHANNEL_LISTENER'] (context, listener) {
        context.getters.echo.channel(listener.channel).listen(listener.event, payload => {
            notifyMe(payload.message, payload.title)
        })
    }

}
