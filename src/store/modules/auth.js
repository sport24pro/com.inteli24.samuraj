import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
    user: null,
    token: Cookies.get('token')
}

// getters
export const getters = {
    user: (state, getters) => {

        if (_.isEmpty(state.user)) {
            //this.$store.dispatch('auth/logout');
            return {};
        }

        return state.user;
    },
    token: state => state.token,
    check: state => !_.isEmpty(state.user),
    isLogged: state => !_.isEmpty(state.user)
}

// mutations
export const mutations = {
    [types.SAVE_TOKEN](state, {token, user, remember}) {
        state.token = token;
        state.user = user;
        Cookies.set('token', token, {expires: remember ? 365 : null});
    },

    [types.FETCH_USER_SUCCESS](state, {user}) {
        state.user = user
    },

    [types.FETCH_USER_FAILURE](state) {
        state.token = null
        Cookies.remove('token')
    },

    [types.LOGOUT](state) {
        state.user = null
        state.token = null
     //   this.clearAll()
       // Cookies.remove('token')
    },

    [types.UPDATE_USER](state, {user}) {
        state.user = user
    }

}

// actions
export const actions = {
    saveToken({commit, dispatch}, payload) {
        commit(types.SAVE_TOKEN, payload)
    },

    async fetchUser({commit}) {
       try {

          const { data } = await axios.get( route('api.user.fetch_user') )
            commit('players/'+types.SAVE_PLAYERS, { players: data.players }, { root: true })
          commit(types.FETCH_USER_SUCCESS, { user: data.user })
        } catch (e) {
          commit(types.FETCH_USER_FAILURE)
        }
    },

    updateUser({commit}, payload) {
        commit(types.UPDATE_USER, payload)
    },


    async logout({commit}) {
        try {


            await axios.post(route('api.user.logout'))
                .then(function (response) {
                    commit(types.LOGOUT);
                }).finally(() => {
                    this.clearAll();
                    Cookies.remove('sport24_session');
                    this.$router.push({name: 'login'});
                });


        } catch (e) {
        }


    },

    async clearAllState({commit}) {

        commit(types.LOGOUT);

    },
    async fetchOauthUrl(ctx, {provider}) {
        const {data} = await axios.post(route('api.user.oauth', {driver: provider}))

        return data.url
    }
}
