import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    groups: {},
    events: {},
    exams: {},
}

// getters
export const getters = {
    groups: state => state.groups,
    isAssignedToGroups: state => _.size(state.groups),
    exams: state => state.exams,
    events: state => state.events,
}


// mutations
export const mutations = {
    [types.SAVE_GROUPS](state, {groups}) {
        state.groups = _.keyBy(groups, 'id');
    },

    [types.ADD_TO_GROUP](state, {group}) {
        state.groups[group.id] = group;
    },

}

// actions
export const actions = {

    async fetchGroups({commit}) {

        const {data} = await axios.get(route('api.user.groups.index'))
        commit(types.SAVE_GROUPS, {groups: data.data})

    },

    addToGroup({commit}, payload) {
        commit(types.ADD_TO_GROUP, payload)
    },
    resignFromGroup({commit}, payload) {
        commit(types.RESIGN_FROM_GROUP, payload)
    },


}
