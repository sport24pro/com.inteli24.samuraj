

const API_ENDPOINT = 'http://fleetcart.pru/';

// auth.js
export const LOGOUT = 'LOGOUT'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'

// lang.js
export const SET_LOCALE = 'SET_LOCALE'

// players.js

export const SAVE_PLAYERS = 'SAVE_PLAYERS'
export const UPDATE_PLAYER = 'UPDATE_PLAYER'

//activities.js

export const SAVE_GROUPS = 'SAVE_GROUPS'
export const ADD_TO_GROUP = 'ADD_TO_GROUP'
export const RESIGN_FROM_GROUP = 'RESIGN_FROM_GROUP'
export const RESIGN_GROUP = 'RESIGN_GROUP'

//cart.js

export const ADD_CART_ITEM = 'ADD_CART_ITEM'
export const UPDATE_CART = 'UPDATE_CART'
export const REMOVE_CART_ITEM = 'REMOVE_CART_ITEM'
export const CLEAR_CART = 'CLEAR_CART'
export const OPEN_CART_SIDEBAR = 'OPEN_CART_SIDEBAR'
export const CLOSE_CART_SIDEBAR = 'CLOSE_CART_SIDEBAR'

export const SAVE_SETTING = 'SAVE_SETTING'


// products

export const SAVE_PRODUCTS = 'SAVE_PRODUCTS'


// events
export const SAVE_EVENTS = 'SAVE_EVENTS'