import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Tabs from '../views/Tabs.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () =>import('@/views/home/index.vue')
  },


  {
    path: '/pages',
    component: Tabs,
    children: [
      {path: ':slug', name: 'pages.show', component: () =>  import('@/views/pages/show.vue') },
  ]
  },

  {
    path: '/account',
    component: Tabs,
    redirect: {name: 'account.index'},
    children: [
      {path: 'index', name: 'account.index', component: () =>  import('@/views/account/index.vue') },
    ]
  },


  {
    path: '/shop',
    component: Tabs,
    redirect: {name: 'shop.index'},
    children: [
      {path: 'index', name: 'shop.index', component: () =>  import('@/views/shop/index.vue') },
      {path: ':slug', name: 'shop.show', component: () =>  import('@/views/shop/show.vue') },
    ]
  },

  {
    path: '/events',
    component: Tabs,
    redirect: {name: 'events.index'},
    children: [
      {path: 'index', name: 'events.index', component: () =>  import('@/views/events/index.vue') },
      {path: ':slug', name: 'show.index', component: () =>  import('@/views/events/show.vue') },

    ]
  },

  {
    path: '/activities',
    component: Tabs,
    children: [
      {path: '', name: 'activities' , redirect: {name: 'activities.index'}},
      {path: 'index', name: 'activities.index', component: import('@/views/activities/index.vue') },

      { path: 'group/:id',  // name: 'registration.index',
        component: import('@/views/activities/group-actions.vue'),


        children: [
          {path: 'description', name: 'registration.description', component: import('@/views/activities/group-description.vue') },
          {path: 'register', name: 'registration.register', component: import('@/views/activities/group-register.vue') ,

            // children: [
            //   {
            //     path: 'return-from-transaction',
            //     name: 'registration.return-from-transaction',
            //     components: {
            //
            //       regi11: import('@/views/activities/return-from-transaction.vue'),
            //
            //     }
            //   },
            //
            //   {path: 'login-user', name: 'registration.login', component: import('@/views/activities/register-form.vue') },
            //   {path: 'register-user', name: 'registration.register_user',  components: {
            //
            //       regi11: import('@/views/activities/register-user.vue'),
            //
            //     } },
            //   {path: 'register-password', name: 'registration.password',
            //     components: {
            //       regi11: import('@/views/activities/register-form.vue'),
            //
            //     } },
            //   {path: 'add_player', name: 'registration.add_player',  components: {
            //       default: import('@/views/activities/add-player.vue'),
            //       regi11: import('@/views/activities/add-player.vue'),
            //
            //     } } ,
            //   {path: 'register_form', name: 'registration.form',
            //     components: {
            //       default: import('@/views/activities/register-form.vue'),
            //       regi11: import('@/views/activities/register-form.vue'),
            //     }
            //   },
            // ]

          },
        ]


      }

    ],
  },



  // {
  //   path: '/WEB_AUTO',
  //   component: Tabs,
  //   children: [
  //
  //
  //     // {
  //     //   path: '/auth',
  //     //   component: AuthDefault,
  //     //   children: [
  //     //     {path: 'login', name: 'login', component: AuthLogin},
  //     //     {path: 'register', name: 'register', component: AuthRegister},
  //     //     {path: 'password', name: 'password', component: AuthRegister},
  //     //     {path: 'password/reset', name: 'password.request', component: AuthPasswordEmail},
  //     //     {path: 'password/reset/:token', name: 'password.reset', component: AuthPasswordReset},
  //     //     {path: 'email/verify/:id', name: 'verification.verify', component: AuthVerificationVerify},
  //     //     {path: 'email/resend', name: 'verification.resend', component: AuthVerificationResend},
  //     //   ]
  //     // },
  //     //
  //     // {
  //     //   path: '/account',
  //     //   component: SettingsDefault,
  //     //   children: [
  //     //     {path: '', redirect: {name: 'settings.profile'}},
  //     //     {path: 'profile', name: 'settings.profile', component: SettingsProfile},
  //     //     //      {path: 'credit-card', name: 'settings.credit-card', component: SettingsCreditCard},
  //     //     {path: 'remove-account', name: 'settings.remove-account', component: SettingsRemoveAccount},
  //     //     {path: 'change-password', name: 'settings.change-password', component: SettingsChangePassword},
  //     //
  //     //
  //     //   ]
  //     // },
  //     //
  //     // {
  //     //   path: '/payments', name: 'user-payments.index',
  //     //   component: Default,
  //     //   children: [
  //     //     {path: '', redirect: {name: 'payments.topay'}},
  //     //     {path: 'to-pay', name: 'payments.topay', component: PaymentsIndex},
  //     //     {path: 'pay', name: 'payments.pay', component: PaymentsIndex},
  //     //     {path: 'history', name: 'payments.history', component: PaymentsIndex}
  //     //   ]
  //     // },
  //
  //     // {
  //     //     path: '/orders',
  //     //     component: Default,
  //     //     children: [
  //     //         {path: '', redirect: {name: 'orders.list'}},
  //     //         {path: 'list', name: 'orders.list', component: OrdersList},
  //     //         {path: 'new', name: 'orders.new', component: OrdersList}
  //     //     ]
  //     // },
  //
  //
  //     // {
  //     //   path: '/groups',
  //     //   component: Default,
  //     //   children: [
  //     //     {path: '', redirect: {name: 'groups.index'}},
  //     //     {path: 'index', name: 'groups.index', component: ActivitiesIndex},
  //     //     {path: 'resignation/:player/:group', name: 'groups.resignation', component: ActivitiesResignation},
  //     //     {path: 'absence-reporting/:player/:group', name: 'groups.absence-reporting', component: ActivitiesAbsence}
  //     //   ]
  //     // },
  //     //
  //     // {
  //     //   path: '/participants',
  //     //   component: Default,
  //     //   children: [
  //     //     {path: '', redirect: {name: 'participants.index'}},
  //     //     {path: 'index', name: 'participants.index', component: ParticipantsIndex},
  //     //     {path: 'new', name: 'participants.new', component: ParticipantsNew},
  //     //     {path: ':id', name: 'participants.edit', component: ParticipantsEdit}
  //     //   ]
  //     // },
  //     //
  //     // {
  //     //   path: '/exams',
  //     //   component: Default,
  //     //   children: [
  //     //     {path: '', redirect: {name: 'exams.index'}},
  //     //     {path: 'index', name: 'exams.index', component: ExamsIndex},
  //     //     {path: ':id', name: 'exams.registration', component: ExamsRegistration}
  //     //   ]
  //     // },
  //     //
  //     //
  //     //
  //
  //
  //
  //
  //   ]
  // },






  // {
  //   path: '/WEB_OWN',
  //   component: Tabs,
  //   children: [
  //     {path: ':page_id', name: 'pages.show', component:  import('@/views/WEB_OWN/Pages.vue') },
  //   ]
  // },


  // {
  //   path: '/tabs/',
  //   component: Tabs,
  //   children: [
  //     {
  //       path: '',
  //       redirect: '/tabs/activities'
  //     },
  //     {
  //       path: '/tabs/activities',
  //       component: () => import('@/views/Activities.vue')
  //     },
  //     {
  //       path: '/tabs/events',
  //       component: () => import('@/views/Events.vue')
  //     },
  //     {
  //       path: '/tabs/shop',
  //       component: () => import('@/views/Shop.vue')
  //     },
  //     {
  //       path: '/tabs/account',
  //       component: () => import('@/views/Account.vue')
  //     }
  //
  //
  //   ]
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
