
export const pageMenuMixin = {


    methods: {

        getTabHref(tab) {

            switch (tab.page_type) {

                case 'WEB_URL':
                    return tab.url;

                case 'WEB_AUTO':
                    return `/${_.get(tab,'page_special.slug','')}`;

                case 'EVENT_CATEGORY':
                    return `/events/category/${_.get(tab,'event_group.slug','')}`;


                case 'PRODUCT_CATEGORY':
                    return `/shop/category/${_.get(tab,'category.slug','')}`;


                case 'WEB_OWN':
                    return `/pages/${_.get(tab,'page.slug','')}`;


            }

            return '';
        }

    },


}
